import { createRouter, createWebHistory } from "vue-router";
import HomePage from "@/views/HomePage.vue";

const routes = [
  {
    path: "/",
    name: HomePage,
    component: HomePage,
  },
  {
    path: "/about",
    name: "about",
    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/AboutPage.vue"),
  },
  {
    path: "/jobs/my",
    name: "jobs-my",
    component: () =>
      import(/* webpackChunkName: "jobs" */ "@/views/jobs/JobsMy.vue"),
  },
  {
    path: "/job/:id",
    name: "jobs-edit",
    component: () =>
      import(/* webpackChunkName: "jobs" */ "@/views/jobs/JobEdit.vue"),
  },
  {
    path: "/jobs/create",
    name: "jobs-create",
    component: () =>
      import(/* webpackChunkName: "jobs" */ "@/views/jobs/JobCreate.vue"),
  },
  {
    path: "/jobs/all",
    name: "jobs-all",
    component: () =>
      import(/* webpackChunkName: "jobs" */ "@/views/jobs/JobsAll.vue"),
  },
  {
    path: "/register",
    name: "register",
    component: () =>
      import(
        /* webpackChunkName: "register" */ "@/views/users/RegisterPage.vue"
      ),
  },
  {
    path: "/login",
    name: "login",
    component: () =>
      import(/* webpackChunkName: "login" */ "@/views/users/LoginPage.vue"),
  },
  {
    path: "/profile",
    name: "profile",
    component: () =>
      import(/* webpackChunkName: "profile" */ "@/views/users/ProfilePage.vue"),
  },
  {
    path: "/:cachAll(.*)*",
    name: "notFound",
    component: () =>
      import(/* webpackChunkName: "notFound" */ "@/views/PageNotFound.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
  linkActiveClass: "active",
});

export default router;
