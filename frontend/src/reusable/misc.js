import { api } from "@/reusable/axios";

export async function deleteItem(id) {
  await api.delete(`/jobs/delete/${id}`);
}

export async function getData(endpoint) {
  await api.get(endpoint).then((resp) => {
    return resp;
  });
}

export function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
