import { reactive } from "vue";

export const currentUser = reactive({
  username: null,
  access_token: null,
  token_type: null,
});

export function clearObject(obj) {
  Object.keys(obj).forEach((key) => {
    obj[key] = null;
  });
}
export function bindObjectToLocalStorage(obj) {
  Object.keys(obj).forEach((key) => {
    localStorage.setItem(key, obj[key]);
  });
}
export function bindLocalStorageToObject(obj) {
  Object.keys(localStorage).forEach((key) => {
    obj[key] = localStorage.getItem(key);
    // obj.key = localStorage.getItem(key); // it makes just key of name 'key'
  });
}

export function errorState() {
  const state = reactive({
    error: "",
    errors: [],
    saved: false,
    loading: false,
  });
  return { state };
}
export function resetObject(obj) {
  Object.keys(obj).forEach((key) => {
    if (typeof obj[key] === "string") obj[key] = "";
    else if (typeof obj[key] === "boolean") obj[key] = false;
    else if (obj[key] instanceof Array) obj[key] = [];
    else obj[key] = null;
  });
}
export function bindErrorsToObject(err, obj) {
  console.log(err);
  const detail = err.response.data.detail;
  if (Array.isArray(detail))
    detail.map((i) => {
      obj.errors[i.loc[1]] = i.msg;
    });
  else obj.error = detail;
}

export function bindObjectToObject(obj1, obj2) {
  Object.keys(obj1).forEach((key) => (obj2[key] = obj1[key]));
}
