import axios from "axios";
import { getToken, removeToken } from '@/reusable/token.js';

export const api = axios.create({
  baseURL: "http://localhost:8000",
  headers: {
    'Content-Type': 'application/json'
    // Authorization: `Bearer ${localStorage.getItem("access_token")}`,
  }
});

api.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error && error.response && error.response.status === 401) {
      removeToken();
    }
    if (error.response) {
      return Promise.reject(error.response);
    }
  }
);

api.interceptors.request.use((config) => {
  const token = getToken();
  if (token) {
    config.headers['Authorization'] = `Bearer ${token}`;
  }

  return config;
});
