from core.hashing import Hasher
from db.models.users import User
from pydantic import EmailStr
from schemas.users import UserCreate
from sqlalchemy import or_
from sqlalchemy.orm import Session
from sqlalchemy.sql import exists


def create_new_user(user: UserCreate, db: Session) -> User:
    user = User(
        username=user.username,
        email=user.email,
        hashed_password=Hasher.get_password_hash(user.password),
        is_active=True,
        is_superuser=False,
    )
    db.add(user)
    db.commit()
    db.refresh(user)
    return user


def get_user_by_email(email: EmailStr, db: Session) -> User:
    user = db.query(User).filter(User.email == email).first()
    return user


def get_user_by_username(username: str, db: Session) -> User:
    user = db.query(User).filter(User.username == username).first()
    return user


def check_if_user_exists(username, email, db) -> bool:
    result = db.query(
        exists().where(or_(User.username == username, User.email == email))
    ).scalar()
    return result


def get_list_of_users(db: Session) -> list[User]:
    users = db.query(User).filter(User.is_active == True).all()  # noqa: E712
    return users


def get_user_by_id(id: int, db: Session) -> User:
    user = db.query(User).filter(User.id == id).first()
    return user


# def get_user_id_by_token(id: int, db: Session) -> User:
#     user = db.query(User).filter(User.id == id).first()
#     return user
