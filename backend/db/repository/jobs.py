from db.models.jobs import Job
from pydantic import ValidationError
from schemas.jobs import JobCreate
from schemas.jobs import JobShow
from sqlalchemy.orm import Session


def create_job_by_object(job: JobCreate, db: Session, owner_id: int) -> Job:
    job_object = Job(**job.dict(), owner_id=owner_id)
    db.add(job_object)
    db.commit()
    db.refresh(job_object)
    return job_object


def get_job_by_id(id: int, db: Session) -> Job:
    job = db.query(Job).filter(Job.id == id).first()
    return job


def get_list_of_jobs(db: Session) -> list[Job]:
    jobs = db.query(Job).filter(Job.is_active == True).all()  # noqa: E712
    return jobs


def get_list_of_jobs_by_owner_id(db: Session, owner_id: int) -> list[Job]:
    jobs = db.query(Job).filter(Job.owner_id == owner_id).all()
    return jobs


def update_job_by_id(id: int, request: JobCreate, db: Session) -> bool:
    job = db.query(Job).filter(Job.id == id)
    try:
        job.update(request.dict(), synchronize_session=False)
    except ValidationError as e:
        print(e)
        return False
    finally:
        db.commit()
        return True


def update_job_by_object(job: Job, request: JobShow, db: Session) -> bool:
    try:
        job.update(request.dict(), synchronize_session=False)
    except ValidationError as e:
        print(e)
        return False
    finally:
        db.commit()
        return True


def delete_job_by_id(id: int, db: Session) -> bool:
    job = db.query(Job).filter(Job.id == id)
    job.delete(synchronize_session=False)
    db.commit()
    return True
