import datetime
from typing import Optional

from pydantic import BaseModel
from pydantic import validator


# this will be used to validate data while creating a Job
class JobCreate(BaseModel):
    title: str
    company: str
    location: str
    description: Optional[str]
    company_url: Optional[str]

    @validator("title")
    def validate_title(cls, v):
        if not v:
            raise ValueError("This field is required")
        if len(v) < 5:
            raise ValueError("Minimum length of title is 5 chars")
        return v


# this will be used to format the response to not to have id, owner_id, ect...
class JobShow(BaseModel):
    title: str
    company: str
    location: str
    description: Optional[str]
    company_url: Optional[str]
    date_posted: Optional[datetime.date]

    # to convert non dict obj to json
    class Config:
        orm_mode = True
