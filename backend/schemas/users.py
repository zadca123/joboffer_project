from pydantic import BaseModel
from pydantic import EmailStr
from pydantic import validator


# properties required during user creation
class UserCreate(BaseModel):
    username: str
    email: EmailStr
    password: str

    @validator("username")
    def validate_username(cls, v):
        if not v:
            raise ValueError("Username is required to create a new account")
        if v and len(v) < 5:
            raise ValueError("Username must be more than 5 characters")
        return v

    @validator("email")
    def validate_email(cls, v):
        if not v:
            raise ValueError("Email address is required to create a new account")
        if "@" and "." not in v:
            raise ValueError("Incorrect email address")
        return v

    @validator("password")
    def validate_password(cls, v):
        if not v:
            raise ValueError("Password is required to create a new account")
        if v and len(v) < 8:
            raise ValueError("Password must be more than 8 characters")
        return v


class UserShow(BaseModel):
    username: str
    email: EmailStr
    is_active: bool

    # tells pydantic to convert even non dict obj to json
    class Config:
        orm_mode = True
