from core.hashing import Hasher
from db.repository.users import create_new_user
from schemas.users import UserCreate
from tests.conftest import db_session


def test_hashing_password():
    password = "testing"
    hashed_password = Hasher.get_password_hash(password)
    assert Hasher.verify_password(password, hashed_password)


def test_hashing_password_during_user_creation():
    password = "testing"
    user_in_create = UserCreate(
        username="John",
        email="john@example.com",
        password=password,
    )
    user = create_new_user(user_in_create, db=db_session)
    assert Hasher.verify_password(password, user.hashed_password)
