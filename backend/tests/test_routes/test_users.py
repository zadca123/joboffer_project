import json

data = {
    "username": "testuser",
    "email": "testuser@example.com",
    "password": "testpassword",
}


def test_create_user(client):
    response = client.post("/users/create", json.dumps(data))
    assert response.status_code == 201
    assert response.json()["email"] == "testuser@example.com"
    assert response.json()["is_active"] == True


def test_user_already_exists(client):
    client.post("/users/create", json.dumps(data))
    response = client.post("/users/create", json.dumps(data))
    assert response.status_code == 409
    assert response.json()["detail"] != ""
