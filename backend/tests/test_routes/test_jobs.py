import json

from fastapi import status

data = {
    "title": "SDE super",
    "company": "doogle",
    "company_url": "www.doogle.com",
    "location": "USA,NY",
    "description": "python",
    "date_posted": "2022-03-20",
}


def test_create_job(client, normal_user_token_headers):
    response = client.post(
        "/jobs/create", json.dumps(data), headers=normal_user_token_headers
    )
    print(response)
    assert response.status_code == status.HTTP_201_CREATED


def test_read_job(client, normal_user_token_headers):
    response = client.post(
        "/jobs/create", json.dumps(data), headers=normal_user_token_headers
    )
    response = client.get("/jobs/get/1")
    assert response.status_code == status.HTTP_200_OK
    assert response.json()["title"] == data["title"]


def test_read_all_jobs(client, normal_user_token_headers):
    client.post("/jobs/create", json.dumps(data), headers=normal_user_token_headers)
    client.post("/jobs/create", json.dumps(data), headers=normal_user_token_headers)
    response = client.get("/jobs/all/")
    assert response.status_code == status.HTTP_200_OK
    assert type(response.json()) == list
    assert len(response.json()) == 2


def test_update_a_job(client, normal_user_token_headers):
    client.post("/jobs/create", json.dumps(data), headers=normal_user_token_headers)
    data["title"] = "test new title"
    response = client.put(
        "/jobs/update/1", json.dumps(data), headers=normal_user_token_headers
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT


def test_update_a_job_get(client, normal_user_token_headers):
    client.post("/jobs/create", json.dumps(data), headers=normal_user_token_headers)
    data["title"] = "test new title other"
    client.put("/jobs/update/1", json.dumps(data), headers=normal_user_token_headers)
    response = client.get("/jobs/get/1")
    assert response.status_code == status.HTTP_200_OK
    assert response.json()["title"] == data["title"]


def test_delete_a_job(client, normal_user_token_headers):
    client.post("/jobs/create", json.dumps(data), headers=normal_user_token_headers)
    response = client.delete("/jobs/delete/1", headers=normal_user_token_headers)
    assert response.status_code == status.HTTP_200_OK


def test_delete_a_job_get(client, normal_user_token_headers):
    client.post("/jobs/create", json.dumps(data), headers=normal_user_token_headers)
    client.delete("/jobs/delete/1", headers=normal_user_token_headers)
    response = client.get("/jobs/get/1")
    assert response.status_code == status.HTTP_404_NOT_FOUND
