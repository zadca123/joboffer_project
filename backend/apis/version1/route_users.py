from db.repository.users import check_if_user_exists
from db.repository.users import create_new_user
from db.repository.users import get_list_of_users
from db.repository.users import get_user_by_id
from db.session import get_db
from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from fastapi import status
from schemas.users import UserCreate
from schemas.users import UserShow
from sqlalchemy.orm import Session

router = APIRouter()


@router.post("/create", response_model=UserShow, status_code=201)
def create_user(user: UserCreate, db: Session = Depends(get_db)):
    if check_if_user_exists(user.username, user.email, db=db):
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="user with this email or username already exist",
        )
    user = create_new_user(user=user, db=db)
    return user


@router.get("/all", response_model=list[UserShow])
def read_users(db: Session = Depends(get_db)):
    users = get_list_of_users(db=db)
    return users


@router.get("/get/{id}", response_model=UserShow)
def read_user(id: int, db: Session = Depends(get_db)):
    user = get_user_by_id(id=id, db=db)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"user with this id {id} does not exist",
        )
    return user
