from apis.version1.route_login import get_current_user_from_token
from db.models.jobs import Job
from db.models.users import User
from db.repository.jobs import create_job_by_object
from db.repository.jobs import delete_job_by_id
from db.repository.jobs import get_job_by_id
from db.repository.jobs import get_list_of_jobs
from db.repository.jobs import get_list_of_jobs_by_owner_id
from db.repository.jobs import update_job_by_id
from db.session import get_db
from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from fastapi import Request
from fastapi import status
from fastapi.templating import Jinja2Templates
from schemas.jobs import JobCreate
from schemas.jobs import JobShow
from sqlalchemy.orm import Session

router = APIRouter()
templates = Jinja2Templates(directory="templates")


@router.post("/create", response_model=JobShow, status_code=201)
def create_job(
    job: JobCreate,
    db: Session = Depends(get_db),
    current_user: User = Depends(get_current_user_from_token),
):
    job: Job = create_job_by_object(job=job, db=db, owner_id=current_user.id)
    print(f"Created job with id: {job.id}")
    return job.__dict__


@router.get("/get/{id}", response_model=JobShow)
def read_job(id: int, db: Session = Depends(get_db)):
    job: Job = get_job_by_id(id=id, db=db)
    if not job:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Job with this id {id} does not exist",
        )
    return job


@router.get("/all", response_model=list[JobShow])
def read_jobs(db: Session = Depends(get_db)):
    jobs: list[Job] = get_list_of_jobs(db=db)
    return jobs


# @router.get("/user/all")
@router.get("/user/all")
def read_jobs_for_current_user(
    db: Session = Depends(get_db),
    current_user: User = Depends(get_current_user_from_token),
):
    jobs: list[Job] = get_list_of_jobs_by_owner_id(db=db, owner_id=current_user.id)
    return jobs


# # response_model=... not works here so dont use!
# okay i now know because response_mode
@router.put("/update/{id}")
def update_job(
    id: int,
    request: JobCreate,
    db: Session = Depends(get_db),
    current_user: User = Depends(get_current_user_from_token),
):
    job = get_job_by_id(id=id, db=db)
    if not job:
        return HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Job with id: {id} does not exist",
        )
    if job.owner_id == current_user.id or current_user.is_superuser:
        update_job_by_id(id=id, request=request, db=db)
        return {"msg": "Successfully updated."}
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED, detail="You are not permitted!!!!"
    )


@router.delete("/delete/{id}")
def delete_job(
    id: int,
    db: Session = Depends(get_db),
    current_user: User = Depends(get_current_user_from_token),
):
    job: Job = get_job_by_id(id=id, db=db)
    if not job:
        return HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Job with id: {id} does not exist",
        )
    if job.owner_id == current_user.id or current_user.is_superuser:
        delete_job_by_id(id=id, db=db)
        return {"msg": "Successfully deleted."}
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED, detail="You are not permitted!!!!"
    )


@router.get("/detail/{id}")
def job_detail(id: int, request: Request, db: Session = Depends(get_db)):
    job = get_job_by_id(id=id, db=db)
    return templates.TemplateResponse(
        "jobs/detail.html", {"request": request, "job": job}
    )
